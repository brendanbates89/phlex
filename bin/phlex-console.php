<?php

use Symfony\Component\Console\Application;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Affinity\Phlex\Command\ServerCommand;

// Grab the Composer Autoload file.
((@include_once __DIR__ . '/../vendor/autoload.php') || @include_once __DIR__ . '/../../../autoload.php');

// Array of Phlex commands.
$commands = array(
    new ServerCommand()
);

// Setup the Console Application
$application = new Application();
$dispatcher = new EventDispatcher();
$application->setDispatcher($dispatcher);

// Test terminate event.
$dispatcher->addListener(ConsoleEvents::TERMINATE, function(ConsoleCommandEvent $event)
{
    $event->getOutput()->writeln("Closing.");
});

foreach($commands as $command)
{
    $application->add($command);
}
$application->run();
 
?>