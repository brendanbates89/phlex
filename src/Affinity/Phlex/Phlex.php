<?php

require 'vendor/autoload.php';

use Phlex\Service\ServiceEngine;
use Phlex\Service\SocketServer;
use Ratchet\WebSocket\WsServer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

$applications = array(
    require 'MyApp/app_bootstrap.php'
);

$serviceHandler = new WsServer(new ServiceEngine($applications));
$wsServer = new SocketServer($serviceHandler, 8081);
$wsServer->Run();