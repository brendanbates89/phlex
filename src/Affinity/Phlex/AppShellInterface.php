<?php

/**
 * This file is part of the Affinity Development 
 * open source toolset.
 * 
 * @author Brendan Bates <brendanbates89@gmail.com>
 * @package Phlex Framework
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */

namespace Phlex;

/**
 * 
 * Application shell interface used for loading an application into
 * the Phlex Framework.
 * 
 * @package Phlex Framework
 * 
 */
interface AppShellInterface 
{
    /*
     * Constructor.  Accepts the application name and service container.
     */
    public function __construct(
        $applicationName,
        array $serviceContainers
    );
    
    /*
     * Returns the application name.
     */
    public function getApplicationName();
    
    /*
     * Returns the service container for a given service name.
     
     * @return ServiceContainer 
     */
    public function findService($serviceName);
}
