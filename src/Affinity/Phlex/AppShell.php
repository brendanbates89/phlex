<?php

/**
 * This file is part of the Affinity Development 
 * open source toolset.
 * 
 * @author Brendan Bates <brendanbates89@gmail.com>
 * @package Phlex Framework
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */

namespace Affinity\Phlex;

use Affinity\Phlex\AppShellInterface;

/**
 * 
 * This is a standard implementation of the AppShell for loading an
 * application into the Phlex Framework.
 * 
 * @package Phlex Framework
 * 
 */
abstract class AppShell implements AppShellInterface
{
    private $applicationName = null;
    private $serviceContainers = array();
    
    public function __construct($name, array $containers)
    {
        $this->applicationName = $name;
        
        foreach($containers as $container)
        {
            $this->serviceContainers[] = $container;
        }
    }
    
    public function getApplicationName()
    {
        return $this->applicationName;
    }
    
    public function findService($serviceName)
    {
        foreach($this->serviceContainers as $container)
        {
            if($container->getServiceName() == $serviceName)
                return $container;
        }
        
        return null;
    }
}
