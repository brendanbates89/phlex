<?php

namespace Affinity\Phlex\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Affinity\Phlex\Service\ServiceEngine;
use Affinity\Phlex\Service\SocketServer;
use Ratchet\WebSocket\WsServer;

/**
 * Console command for starting the Phlex Server.
 */
class ServerCommand extends Command
{
    private $input;
    private $output;

    protected function configure()
    {
        $this->setName('phlex:Server')
             ->setDescription('Start the Phlex Websocket Server.')
             ->addArgument(
                'action',
                InputArgument::REQUIRED,
                'Action to perform on the current running server.'
             );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $this->input = $input;
        $this->output = $output;
        
        $action = $input->getArgument('action');        
        switch($action)
        {
            case 'Start':
                $this->startServer();
                break;
                
            case 'Stop':
                break;
                
            case 'Restart':
                break;
        }
    }
    
    private function startServer()
    {    
        // Initiate the Phlex ServiceEngine.
        $serviceEngine = new ServiceEngine(array());
        
        // Create a WebSockets session using the ServiceEngine, and create
        // a socket server.
        $phlexServer = new SocketServer($serviceEngine, 8081);
        
        // Notify user that the server task is starting.
        $this->output->writeln("The Phlex Websocket Server is starting.  The Console will be unresponsive to ");
        $this->output->writeln("user input during this time.  Use the `phlex:Server Stop` command to stop the");
        $this->output->writeln(" server.\n");
        
        // Run the server.
        $phlexServer->Run();
    }
}

?>