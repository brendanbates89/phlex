<?php

/**
 * This file is part of the Affinity Development 
 * open source toolset.
 * 
 * @author Brendan Bates <brendanbates89@gmail.com>
 * @package Phlex Framework
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */

namespace Phlex\Service\Model;

/**
 * 
 * Class Description.
 * 
 * @package Phlex Framework
 * 
 */
interface ServiceHandlerInterface 
{
    /*
     * Loads data from a service into the current user context.
     * 
     * @return ServiceObject
     */
    public function Load(ServiceObjectInterface $object, array $attributes);
    
    /*
     * Saves a ServiceObject from a UserContext.  Should be for saving
     * existing records only.
     */
    public function Save(ServiceObjectInterface $object, array $attributes);
    
    /*
     * Adds a new ServiceObject.
     */
    public function Add(ServiceObjectInterface $object, array $attributes);
    
    /*
     * Deletes a ServiceObject.
     */
    public function Delete(ServiceObjectInterface $object, array $attributes);
    
}
