<?php

/**
 * This file is part of the Affinity Development 
 * open source toolset.
 * 
 * @author Brendan Bates <brendanbates89@gmail.com>
 * @package Phlex Framework
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */

namespace Phlex\Service\Model;

/**
 * 
 * Class Description.
 * 
 * @package Phlex Framework
 * 
 */
interface ServiceObjectInterface 
{
    /*
     * 
     */
    public function ServiceObject($objectType);
    
    /*
     * 
     */
    public function setFieldValue($fieldName, $value);
    
    /*
     * 
     */
    public function getFieldValue($fieldName);
    
    /*
     * 
     */
    public function setPrimaryKey($keyName, $value);
    
    /*
     * 
     */
    public function getPrimaryKey($keyName);
    
    /*
     * 
     */
    public function setAttribute($attributeName, $value);
    
    /*
     * 
     */
    public function getAttribute($attributeName);
    
    /*
     * 
     */
    public function getObjectType();
}
