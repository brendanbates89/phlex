<?php

/**
 * This file is part of the Affinity Development 
 * open source toolset.
 * 
 * @author Brendan Bates <brendanbates89@gmail.com>
 * @package Phlex Framework
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */

namespace Phlex\Service\Model;

use Phlex\Service\Model\ServiceHandlerInterface;

/**
 * 
 * The ServiceContract provides a way to bind a type of object to a handler, and
 * define the data which is provided.
 * 
 * @package Phlex Framework
 * 
 */
interface ServiceContractInterface 
{
    /*
     * 
     */
    public function __construct(ServiceHandlerInterface $handler, $objectType);
    
    /*
     * 
     */
    public function getPrimaryKeyList();
    
    /*
     * 
     */
    public function getAttributeList();
    
    /*
     * 
     */
    public function getFieldList();
    
    /*
     * 
     */    
    public function getServiceHandler();
    
    /*
     * 
     */
    public function getObjectType();
}
