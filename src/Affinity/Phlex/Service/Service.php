<?php

/**
 * This file is part of the Affinity Development 
 * open source toolset.
 * 
 * @author Brendan Bates <brendanbates89@gmail.com>
 * @package Phlex Framework
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */

namespace Affinity\Phlex\Service;

use Affinity\Phlex\Service\ServiceInterface;
use Affinity\Phlex\Service\ServiceContractInterface;

/**
 * 
 * Class Description.
 * 
 * @package Phlex Framework
 * 
 */
abstract class Service implements ServiceInterface
{
    private $contracts = array();
    private $name;
    
    public function __construct($serviceName, array $serviceContracts) 
    {
        $this->name = $serviceName;
        
        foreach($serviceContracts as $contract)
        {
            if($contract instanceof ServiceContractInterface)
                $this->contracts[] = $contract;
        }
    }

    public function findServiceContract($objectType)
    {
        foreach($this->contracts as $contract)
        {
            if($contract->getObjectType() == $objectType)
                return $contract;
        }
        
        return null;
    }

    public function getServiceName() 
    {
        return $this->name;
    }    
}
