<?php

/**
 * This file is part of the Affinity Development 
 * open source toolset.
 * 
 * @author Brendan Bates <brendanbates89@gmail.com>
 * @package Phlex Framework
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */

namespace Phlex\Service;

use Phlex\Model\ServiceObjectInterface;

/**
 * 
 * Class Description.
 * 
 * @package Phlex Framework
 * 
 */
abstract class ServiceObject implements ServiceObjectInterface
{
    
}
