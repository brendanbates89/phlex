<?php

namespace Affinity\Phlex\Service;

use Exception;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use SplObjectStorage;

class ServiceEngine implements MessageComponentInterface
{
    /**
     * Current repository of clients connected to the ServiceEngine.
     */
    protected $clients;
    
    /**
     * Array of applications running under the ServiceEngine.
     */
    protected $applications = array();

    public function __construct(array $apps)
    {
        foreach($apps as $app)
        {
            if($app instanceof AppShellInterface)
                $this->applications[] = $app;
        }
            
        $this->clients = new SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $serviceResolved = false;
        
        // Get the URL for the service endpoint.
        $service = $this->parseServiceEndpoint($conn->WebSocket->request->getUrl());

        // Attempt to locate the service.
        $currentApp = null;
        foreach($this->applications as $app)
        {
            if($app->getApplicationName() == $service["application"])
            {
                $service = $app->findService($service["service"]);
                if($service != null)
                {
                    $serviceResolved = true;
                    
                    $clientContext = new ClientContext($service);
                    $this->clients->attach($conn, $clientContext);
                }
                break;
            }
        }
        
        // Check if the service has been resolved.
        if(!$serviceResolved)
        {
            echo "The service endpoint you are attempting to reach does not exist.\n";
            $data = '{"ServiceResponse":{"Exception": {"Type": "ServiceEndpointException", ' . 
                    '"Message": "The service endpoint you are attempting to reach does not exist."}}}';
            $conn->send($data);
            $conn->close();
            return;
        }
        else
        {
            echo "Client connection " . $conn->resourceId . " started.\n";
        }
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $numRecv = count($this->clients) - 1;
        echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');

        foreach ($this->clients as $client) {
            if ($from !== $client) {
                // The sender is not the receiver, send to each client connected
                $client->send($msg);
            }
        }
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
    
    /*
     * Parses a service endpoint into the application name and service name.
     */
    private function parseServiceEndpoint($serviceEndpoint)
    {
        $serviceEndpoint = parse_url($serviceEndpoint, PHP_URL_PATH);        
        $endpointDir = explode("/", $serviceEndpoint);
        
        $returnArr = array(
            "application" => null,
            "service" => null
        );
        
        // Attempt to get the URL data from the requesting client.
        if(isset($endpointDir[1]) && isset($endpointDir[2]))
        {
            $baseServiceDir = $endpointDir[1];
            $serviceExt = $endpointDir[2];
        }
        
        // Check to make sure the request is for the Phlex ServiceEngine.
        if($baseServiceDir == "ServiceEngine")
        {
            // Parse the application name and service name from the
            // service extension.
            $serviceExt = explode(":", $serviceExt, 2);
            
            if(count($serviceExt == 2))
            {
                $returnArr["application"] = $serviceExt[0];
                $returnArr["service"] = $serviceExt[1];
            }
        }
        
        return $returnArr;
    }
}