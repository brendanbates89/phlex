<?php

/**
 * This file is part of the Affinity Development 
 * open source toolset.
 * 
 * @author Brendan Bates <brendanbates89@gmail.com>
 * @package Phlex Framework
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */

namespace Phlex\Service\Model;

/**
 * 
 * Class Description.
 * 
 * @package Phlex Framework
 * 
 */
interface ServiceInterface
{
    /*
     * 
     */
    public function __construct($serviceName, array $serviceContracts);
    
    /*
     * 
     */
    public function getServiceName();
    
    /*
     * 
     */
    public function findServiceContract($objectType);
}
