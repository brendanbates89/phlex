<?php

namespace Affinity\Phlex\Service;

use Ratchet\Server\IoServer;
use Ratchet\Websocket\WsServer;
use Ratchet\Http\HttpServer;
use Affinity\Phlex\Service\ServiceEngine;

/**
 * This class is used to run an instance of the SocketServer.
 */
class SocketServer
{
    /* @var $server IoServer */
    private $server;
    
    /**
     * Constructs an instance of the SocketServer. 
     *
     * @param $serviceEngine The ServiceEngine object 
     */
    function __construct(ServiceEngine $serviceEngine, $port = 80, $address = '0.0.0.0')
    {
        $wsServer = new WsServer($serviceEngine);
        $httpServer = new HttpServer($wsServer);
        $this->server = IoServer::factory($httpServer, $port, $address);
    }
    
    public function Run()
    {
        echo "Server running!";
        $this->server->run();
    }
}

?>
