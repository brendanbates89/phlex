<?php

/**
 * This file is part of the Affinity Development 
 * open source toolset.
 * 
 * @author Brendan Bates <brendanbates89@gmail.com>
 * @package Phlex Framework
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */

namespace Phlex\Service;

/**
 * 
 * Class Description.
 * 
 * @package Phlex Framework
 * 
 */
class ClientContext 
{
    private $service;
    private $objectStore;
    
    public function __construct($service)
    {
        $this->service = $service;
    }
}
